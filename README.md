## Disroot theme for Foregejo

Customizing Gitea is typically done using the `CustomPath` folder - by default this is the `custom` folder from the running directory of Forgejo, but it may be different if your build has set this differently. This is the central place to override configuration settings, templates, etc. You can check the `CustomPat`h using `gitea help`. You can also find the path on the Configuration tab in the Site Administration page. You can override the `CustomPath` by setting either the `GITEA_CUSTOM` environment variable or by using the `--custom-path` option on the gitea binary. (The option will override the environment variable.)

In this `custom` folder, create a `public\css` folder then, to use this theme, `git clone` it in `custom/public/css`. It will create copy the css files of our Beetroot themes.

Then edit `app.ini` and edit the `THEMES` var like this:
`THEMES = beetroot-light,beetroot-dark,beetroot-auto` (you can also keep the themes already dfined there).

Don't forget to restart gitea: `service gitea restart`

To know more about customizing Forgejo, check https://docs.gitea.io/en-us/customizing-gitea/#customizing-the-look-of-gitea

test5